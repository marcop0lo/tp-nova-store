(function (angular) {
    'use strict';
    angular.module('ngAppDemo', []).controller('ngAppDemoController', function ($scope) {
        $scope.a = 1;
        $scope.b = 2;
        $scope.activeView = 'login';

        $scope.sumar = function(){
            $scope.a++;
            $scope.b++;
        }

        $scope.login = function(){
            $scope.activeView = 'products';
        }
    });
})(window.angular);